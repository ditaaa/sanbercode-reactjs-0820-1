//soal 1
console.log("----SOAL 1-----")
const luas = (pi,r) => {
    return pi*r*r
}
console.log("luas lingkaran dari jari-jari 14 adalah " + luas(3.14,14))

const keliling = (pi,r) => {
    return 2*pi*r
}
console.log("keliling lingkaran dari jari-jari 7 adalah " + keliling(3.14,7))


console.log("\n")
//soal 2
console.log("----SOAL 2-----")
let kalimat = ""

let kalimat1 = 'saya'
let kalimat2 = 'adalah'
let kalimat3 = 'seorang'
let kalimat4 = 'frontend'
let kalimat5 = 'developer'
 
let string = `${kalimat1} ${kalimat2} ${kalimat3} ${kalimat4} ${kalimat5}`
 
console.log(string)


console.log("\n")
//soal 3
console.log("----SOAL 3-----")
const newFunction = function literal(firstName, lastName){
    return {
      firstName: firstName,
      lastName: lastName,
      fullName: () => {
        console.log(`${firstName} ${lastName}`)
      }
    }
  }
   
//Driver Code 
newFunction("William", "Imoh").fullName()


console.log("\n")
//soal 4
console.log("----SOAL 4-----")
const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
  }
  const {firstName, lastName, destination, occupation, spell} = newObject

// Driver code
console.log(firstName, lastName, destination, occupation)


console.log("\n")
//soal 5
console.log("----SOAL 5-----")
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west, ...east]

//Driver Code
console.log(combined)
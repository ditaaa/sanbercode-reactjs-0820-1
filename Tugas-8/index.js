var readBooks = require("./callback.js");

var books = [
  { name: "LOTR", timeSpent: 3000 },
  { name: "Fidas", timeSpent: 2000 },
  { name: "Kalkulus", timeSpent: 4000 },
  { name: "komik", timeSpent: 1000 }
];

var WaktuTersedia = 10000;

 readBooks(WaktuTersedia, books[0], function(time){
     readBooks(time, books[1], function(time){
         readBooks(time, books[2], function(time){
            readBooks(time, books[3], function(time){
                return time;
            })
        })
    })
});
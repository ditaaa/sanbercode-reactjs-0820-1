//soal 1
console.log("----SOAL 1-----")
function halo() {
    return "Halo Sanbers!"
}

console.log(halo()) // "Halo Sanbers!"
console.log()


//soal 2
console.log("----SOAL 2-----")
function kalikan(x, y) {
    return x * y
}

var num1 = 12
var num2 = 4
 
var hasilKali = kalikan(num1, num2)
console.log(hasilKali) // 48
console.log()


//soal 3
console.log("----SOAL 3-----")
function introduce(name, age, address, hobby) {
    var kalimat = "Nama saya " + name + ", umur saya " + age + " tahun, alamat saya di " + address + ", dan saya punya hobby yaitu " + hobby + "!"
    return kalimat
}

var name = "John"
var age = 30
var address = "Jalan belum jadi"
var hobby = "Gaming"
 
var perkenalan = introduce(name, age, address, hobby)
console.log(perkenalan) // Menampilkan "Nama saya John, umur saya 30 tahun, alamat saya di Jalan belum jadi, dan saya punya hobby yaitu Gaming!" 
console.log()


//soal 4 
console.log("----SOAL 4-----")
var DaftarPesertaArr = ["Asep", "laki-laki", "baca buku" , 1992]
var DaftarPesertaObj = {
    Nama : "Asep",
    JenisKelamin : "laki-laki",
    Hobi : "baca buku",
    TahunLahir : 1992
}
console.log(DaftarPesertaObj);
console.log()


//soal 5
console.log("----SOAL 5-----")
var buah = [
    {
        nama : "strawberry",
        warna : "merah",
        adaBijinya : false,
        harga : 9000, 
    },
    {
        nama : "jeruk",
        warna : "oranye",
        adaBijinya : true,
        harga : 8000,
    },
    {
        nama : "Semangka",
        warna : "Hijau&Merah",
        adaBijinya : true,
        harga : 10000,
    },
    {
        nama : "Pisang",
        warna : "Kuning",
        adaBijinya : false,
        harga : 5000,
    } 
]
  console.log(buah[0]);
  console.log()


//soal 6
console.log("----SOAL 6-----")
var dataFilm = []
function tambahData(nama, durasi, genre, tahun) {
  dataFilm.push({
    nama: nama,
    durasi: durasi,
    genre: genre,
    tahun: tahun,
  });
}

tambahData("Coraline", "100 menit", "Fantasi", "2009");
tambahData("Monster Inc", "120 menit", "Fantasi", "2001");

console.log(dataFilm);
console.log()
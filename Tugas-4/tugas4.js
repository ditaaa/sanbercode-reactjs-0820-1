// soal 1
console.log("LOOPING PERTAMA");
var x = 2;
while (x <= 20) {
    console.log(x + " - I love coding");
    x += 2;
}
console.log("LOOPING KEDUA");
var x = 20;
while (x >= 2) {
    console.log(x + " - I will become a frontend developer");
    x -= 2;
}


// soal 2
for(var x = 1; x <= 20; x++) {
    if (x % 3 === 0 && x % 2 === 1){
      console.log(x + " - I Love Coding")
    }else if(x % 2 === 1){
      console.log(x + " - Santai")
    }else{
      console.log(x + " - Berkualitas")
    }
  } 


// soal 3
for(var x = 1; x <= 7; x++) {
    var z = '';
    for(var y = 1; y <= x; y++) {
      z = z + '#';
    }
    console.log(z);
  }


// soal 4
var kalimat = "saya sangat senang belajar javascript";

console.log(kalimat.split(" "));


// soal 5
var daftarBuah = ["2. Apel", "5. Jeruk", "3. Anggur", "4. Semangka", "1. Mangga"];

var sortdaftarBuah = daftarBuah.sort();
for (var x = 0; x < daftarBuah.length; x++) {
    console.log(daftarBuah[x]);
}